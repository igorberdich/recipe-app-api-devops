#!/bin/sh

set -e

# to collect all static files in one directory
python manage.py collectstatic --noinput
#wait for database to start
python manage.py wait_for_db
#run all existing db upgrades
python manage.py migrate

uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi