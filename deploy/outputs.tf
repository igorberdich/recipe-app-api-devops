output "db_host" {
  value = aws_db_instance.main.address
}

output "bastian_host" {
  value = aws_instance.bastion.public_dns
}

# for load balancer
output "api_endpoint" {
  value = aws_route53_record.app.fqdn #aws_lb.api.dns_name
}