terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket         = "dg-dgmc-tfstate"
    key            = "dg-dgmc.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "dg-dgmc-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-2"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}

