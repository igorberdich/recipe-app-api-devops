variable "prefix" {
  default = "dg"
}

variable "project" {
  default = "dg-bp-dgcomm-docker"
}

variable "contact" {
  default = "iberdichevsky@digitalguardian.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "303646959396.dkr.ecr.us-east-2.amazonaws.com/dgregistry:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "303646959396.dkr.ecr.us-east-2.amazonaws.com/dgregistry:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

# to retrieve zone name from account
variable "dns_zone_name" {
  description = "Domain name"
  default     = "digitalguardian.net"
}

# to split environments to different subdomains
variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}